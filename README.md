# Worcket Challenge

## Getting started

```
npm install
npm start 
curl -v  http://admin:supersecret@localhost:3000/people
```

Puede pasar el LOG_LEVEL como variable de entorno. Puede ser info, error o debug 
```
LOG_LEVEL=debug npm start
```

## Ejecutar el contenedor 

```
docker build --tag challenge .
mkdir /tmp/stats
docker run -it --rm -v /tmp/stats:/usr/src/app/log -e LOG_LEVEL=debug -p 3000 --name challenge challenge
```

Las estatidisticas estan disponibles en el archivo /tmp/stats/stats.log

## Ejecución en producción

Puede crear multiple docker containers e incluir un containe de nginx para balancear la carga entre ellos

Para gestionar los logs de los contenedores, puede instalar plugins a docker con drivers para logs

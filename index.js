'use strict'
const express = require('express')
const app = express()
// Support Authentication. User admin pass: supersecret
const basicAuth = require('express-basic-auth')
const fetch = require('node-fetch')
// Stats
const requestStats = require('request-stats')
// Logging
const log = require('winston')
const { LOG_LEVEL } = process.env
log.loggers.add('log', {
  transports: [
    new log.transports.Console({
      level: 'info',
      silent: LOG_LEVEL !== 'info'
    }),
    new log.transports.Console({
      level: 'error',
      silent: LOG_LEVEL !== 'error'
    }),
    new log.transports.Console({
      level: 'debug',
      silent: LOG_LEVEL !== 'debug'
    })]
})
log.loggers.add('stats', {
  transports: [
    new log.transports.File({ filename: 'stats.log' })
  ]
})
const logger = log.loggers.get('log')
const statslogger = log.loggers.get('stats')

var data = []

// Load cache on memory
var cache = ''
const loadCache = async (url = 'https://swapi.co/api/people/') => {
  let sections = [
    ['films', 'title'],
    ['species', 'name'],
    ['vehicles', 'name'],
    ['starships', 'name']
  ]
  // Get homeword name from endpoint
  const getPlanetName = async (url) => {
    let planet

    // Collect Planet info
    await fetch(url)
      .then(res => res.json())
      .then(json => {
        planet = json.name
        logger.debug('Get planet ' + planet + ' info')
        return planet
      })
      .catch(err => {
        logger.error(err)
      })
    return planet
  }
  let getRemoteData = await fetch(url)
    .then(res => res.json())
    .catch(err => {
      logger.error(err)
    })
  const getRemoteName = async (url, section) => {
    let data = await fetch(url)
      .then(res => res.json())
      .then(json => json[section])
      .catch(err => {
        logger.error(err)
        process.exit(1)
      })
    return data
  }
  data = data.concat(
    getRemoteData.results.map(async (ppl, indx) => {
      let attributes = {
        name: ppl.name,
        id: ppl.url.match(/(?:people\/)(\d+?)\/$/)[1],
        height: ppl.height,
        mass: ppl.mass,
        hair_color: ppl.hair_color,
        skin_color: ppl.skin_color,
        eye_color: ppl.eye_color,
        birth_year: ppl.birth_year,
        gender: ppl.gender,
        homeworld: {
          id: ppl.homeworld.match(/(?:planets\/)(\d+?)\/$/)[1],
          name: await getPlanetName(ppl.homeworld)
        }
      }

      sections.forEach(section => {
        attributes[section[0]] = ppl[section[0]].map(async urlpath => {
          let reg = '(?:' + section[0] + '/)(\\d+?)/$'
          let re = new RegExp(reg)
          let values = {
            id: urlpath.match(re)[1],
            name: await getRemoteName(urlpath, section[1])
          }

          return values
        })
        Promise.all(attributes[section[0]]).then(result => {
          attributes[section[0]] = result
        }).catch(err => {
          logger.error(err)
        })
      })

      return attributes
    }))

  if (getRemoteData.next) { loadCache(getRemoteData.next) } else {
    cache = await Promise.all(data)
    app.emit('ready')
  }
}
logger.info('Loading cache')
loadCache()

// Allow CORS
app.use((req, res, next) => {
  requestStats(req, res, stats => {
    statslogger.info(stats)
  })

  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.on('ready', () => {
  logger.debug('Loaded total ' + cache.length)
  logger.info('Starting server')
  app.get('/people', (req, res) => {
    res.json(cache.map((ppl) => {
      return { name: ppl.name, id: ppl.id }
    }))
  })
  app.get('/people/:id', (req, res, next) => {
    logger.info('Requested info for ID' + req.params.id)
    let result = cache.filter(ppl => ppl.id === req.params.id)
    if (result.length > 0) { res.json(result) } else {
      logger.info('Id not found')
      res.status(404).send({ message: 'ID not found' })
    }
  })
  app.listen(3000)
})

app.use(basicAuth({
  users: { 'admin': 'supersecret' }
}))
